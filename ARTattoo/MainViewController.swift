//
//  ViewController.swift
//  ARTattoo
//
//  Created by Евгения Кирюшина on 04.12.2019.
//  Copyright © 2019 Евгения Кирюшина. All rights reserved.
//

import UIKit
import ARKit
import CoreML
import Vision
import AVFoundation
import SpriteKit

class ViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {

    lazy var recognizer = MLRecognizer(
        model: InfinityClassifier().model,
        sceneView: sceneView
    )
    
    let detectionImages = ARReferenceImage.referenceImages(
        inGroupNamed: "AR Resources",
        bundle: nil
    )
    
    var isFound = false
    
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var arButton: UIButton!
    @IBOutlet weak var tattooView: UIImageView!
    @IBOutlet weak var sizeSlider: UISlider!
    
    let tattooNode = SCNNode(geometry: SCNPlane(width: 2, height: 2))
    let backCamera = ARWorldTrackingConfiguration()
    let frontCamera = ARFaceTrackingConfiguration()
    var arButtColor: UIColor = .black
    var isAROn = true
    var j = 0
    var k = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sceneView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tattooNode.name = "Moon"
        arButtColor = arButton.tintColor
        let rotation = SCNMatrix4MakeRotation(Float.pi / 2, 0, 0, 1)
        let transform = SCNMatrix4Mult(tattooNode.transform, rotation)
        
        tattooNode.transform = transform
        resetTracking()
    }
    
    func resetTracking() {
        backCamera.detectionImages = detectionImages
        backCamera.maximumNumberOfTrackedImages = 1
        backCamera.isLightEstimationEnabled = true
        backCamera.isAutoFocusEnabled = true
        backCamera.automaticImageScaleEstimationEnabled = true
        sceneView.session.run(backCamera, options: [.resetTracking, .removeExistingAnchors])
    }
    // MARK: - SCNSceneRendererDelegate
    func renderer(_ renderer: SCNSceneRenderer, didRenderScene scene: SCNScene, atTime time: TimeInterval) {
//        print(#function, sceneView.session.currentFrame)
        if sceneView.session.currentFrame != nil && k % 2 == 0{
            setupRecognition()
        }
        k += 1
    }

    func setupRecognition(){
        if let currentFrame = sceneView.session.currentFrame {
            DispatchQueue.global(qos: .background).async {
                do {
                    let model = try VNCoreMLModel(for: InfinityClassifier().model)
                    let request = VNCoreMLRequest(model: model, completionHandler: { (request, error) in
                        // Jump onto the main thread
                        DispatchQueue.main.async {
                            // Access the first result in the array after casting the array as a VNClassificationObservation array
                            guard let results = request.results as? [VNClassificationObservation], let result = results.first else {
                                print ("No results?")
                                return
                            }

                            // Create a transform with a translation of 0.2 meters in front of the camera
                            var translation = matrix_identity_float4x4
                            translation.columns.3.z = -0.4
                            let transform = simd_mul(currentFrame.camera.transform, translation)

                            // Add a new anchor to the session
                            let anchor = ARAnchor(transform: transform)

                            // Set the identifier
                            ARBridge.shared.anchorsToIdentifiers[anchor] = result.identifier

                            if result.identifier == "found"{
                                self.isFound = true
                                print ("FOUND!!!")
                            } else {
                                self.isFound = false
                                print("not found")
                                self.tattooNode.removeFromParentNode()
                            }
                            let scaleAR = CGFloat(self.sizeSlider.value/6 + 0.055)
                            self.tattooNode.scale = .init(scaleAR, scaleAR, 0.055)
                            self.sceneView.session.add(anchor: anchor)

                        }
                    })
                    let handler = VNImageRequestHandler(cvPixelBuffer: currentFrame.capturedImage, options: [:])
                    try handler.perform([request])
                } catch {}
            }
        }
    }
    
    // Slider's work
    @IBAction func changeSizeOfTattoo(_ sender: UISlider) {
        let scaleImg = CGFloat(sender.value + 0.2)
        let transform = CGAffineTransform.init(scaleX: scaleImg, y: scaleImg)
        tattooView.transform = transform
    }
    
    // AR button
    @IBAction func turnARonOrOff(_ sender: Any) {
        if isAROn {
            arButton.tintColor = UIColor.gray.withAlphaComponent(0.3)
            tattooNode.isHidden = true
            tattooView.image = UIImage(named:"moon")
            isAROn = false
        } else {
            arButton.tintColor = arButtColor
            tattooView.image = nil
            tattooNode.isHidden = false
            isAROn = true
        }
    }
    
    // change camera button
    @IBAction func changeCamera(_ sender: UIButton) {
        if j % 2 == 0 {
            sceneView.session.run(frontCamera, options: [.resetTracking, .removeExistingAnchors])
        } else {
            sceneView.session.run(backCamera, options: [.resetTracking, .removeExistingAnchors])
        }
        j+=1
    }
    
    // make photo button
    @IBAction func makePhoto(_ sender: UIButton) {
        let img = sceneView.snapshot()
        sceneView.session.pause()
        
        let alert = UIAlertController(title: "Do you want to save this photo?", message: "", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil)
            self.sceneView.session.run(self.sceneView.session.configuration!, options: [.resetTracking, .removeExistingAnchors])
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
            UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil)
            self.sceneView.session.run(self.sceneView.session.configuration!, options: [.resetTracking, .removeExistingAnchors])
        }))

        self.present(alert, animated: true)
    }
    
    // Tattoo button
    @IBAction func showLibrary(_ sender: UIButton) {
        
    }
}

extension ViewController: ARSCNViewDelegate {

    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if isFound && node.childNode(withName: "Moon", recursively: false) == nil{
             print(anchor.classForCoder)

//            guard let imageAnchor = anchor as? ARImageAnchor else { return }
//            addIndicatorPlane(to: imageAnchor)
//
//            // send off anchor to be screenshot and classified
//            recognizer.classify(imageAnchor: imageAnchor) { [weak self] result in
//                if case .success(let classification) = result {
//                    // update app with classification
            self.attachTattoo(to: node, anchor: anchor)
//                }
//            }
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        print(anchor.transform)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        node.childNode(withName: "Moon", recursively: false)?.removeFromParentNode()
    }

}
class ARBridge {

    static let shared = ARBridge()

    var anchorsToIdentifiers = [ARAnchor : String]()

}
extension ViewController {

    func addIndicatorPlane(to imageAnchor: ARImageAnchor) {
        let node = sceneView.node(for: imageAnchor)
        let size = imageAnchor.referenceImage.physicalSize
        let geometry = SCNPlane(width: size.width, height: size.height)
        let plane = SCNNode(geometry: geometry)
        plane.geometry?.firstMaterial?.diffuse.contents = UIColor.darkGray
        plane.geometry?.firstMaterial?.fillMode = .lines
        plane.eulerAngles.x = -.pi / 2
        node?.addChildNode(plane)
    }
    
//    func addIndicPlane (anchor: ARAnchor){
//        let node = sceneView.node(for: anchor)
////        let size = imageAnchor.referenceImage.physicalSize
//        let posX = anchor.transform.columns.0.sum()
//        let posY = anchor.transform.columns.1.y
//        let posZ = anchor.transform.columns.2.z
//        let ygol = anchor.transform.columns.3
//        let geometry = SCNPlane(width: 2, height: 2)
//        let plane = SCNNode(geometry: geometry)
//        plane.geometry?.firstMaterial?.diffuse.contents = UIColor.darkGray
////        plane.geometry?.firstMaterial?.fillMode = .lines
//        plane.eulerAngles.x = -.pi / 2
//        plane.position.x = posX
//        plane.position.y = posY
//        plane.position.z = posZ
//
//        print("x: \(posX), \n y: \(posY), \n z: \(posZ), \n angle: \(ygol)")
////        plane.position.self = node!.worldPosition
//        node?.addChildNode(plane)
//    }

    // Adds a tattoo
    func attachTattoo(to node: SCNNode, anchor: ARAnchor) {
        tattooNode.position = node.position
        tattooNode.geometry!.firstMaterial!.diffuse.contents = UIImage(named: "moon")
        
        
//        let posX = anchor.transform.columns.0.sum()
//        let posY = anchor.transform.columns.1.sum()
//        let posZ = anchor.transform.columns.2.sum()
//        tattooNode.position = .init(posX,posY, posZ)
        
        
//        let animation = CABasicAnimation(keyPath: "transform")
//        animation.fromValue = tattooNode.transform
//        animation.toValue = scnScene.rootNode.convertTransform(newTransform,from: nil)
//        animation.duration = 0.5
//        tattooNode.addAnimation(animation, forKey: nil)
//
//        tattooNode.rotation.
        
//        tattooNode.eulerAngles.x = -.pi / 2
//        let box = tattooNode.boundingBox
//        tattooNode.pivot.m41 = (box.max.x - box.min.x) / 2.0
//        tattooNode.position.z = node.boundingBox.max.z + 0.012 // 1 cm below card
//        tattooNode.position.y = -1.0
        node.addChildNode(tattooNode)
    }

    func updateTattoo(_ tattooNode: SCNNode, to node: SCNNode){        
    }
}
